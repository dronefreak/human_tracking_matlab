#!/bin/bash

DIR="/home/iisc/Desktop/master/CameraImages"

while true
do 
files=$(shopt -s nullglob dotglob; echo $DIR/*) 
	if (( ${#files} ))
	then
		cd $DIR
		for file in * 
		do
			echo "$DIR has something"
			mv "$file" "ptzimage.jpg" 

	echo "Executing matlab now"
	sudo env LD_PRELOAD=/usr/lib/x86_64-linux-gnu/libstdc++.so.6  /usr/local/MATLAB/R2016a/bin/matlab -nodesktop -r "run /home/iisc/Desktop/master/demo1.m;quit;"
	echo "Done executing matlab"
		
			rm *
		done
	else
        	echo "$DIR is Empty"
	fi
sleep 1
done
