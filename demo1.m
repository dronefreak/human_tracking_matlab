
function demo1()

delete('MATLAB_logs.txt')

startup;

if ~exist('no_compile', 'var')
  fprintf('compiling the code...');
  compile;
  fprintf('done.\n\n');
end

% load('VOC2007/car_final');
% model.vis = @() visualizemodel(model, ...
%                   1:2:length(model.rules{model.start}));
% test('000034.jpg', model, 1);

load('INRIA/inriaperson_final');
model.vis = @() visualizemodel(model, ...
                  1:2:length(model.rules{model.start}));
test('/home/fake_taxi/master/CameraImages/100028.jpg', model, 2);

load('VOC2007/person_grammar_final');
model.class = 'person grammar';
model.vis = @() visualize_person_grammar_model(model, 6);
test('/home/fake_taxi/master/CameraImages/100028.jpg', model, 2);

load('VOC2010/dog_final');
model.vis = @() visualizemodel(model, ...
                  1:2:length(model.rules{model.start}));
test1('/home/fake_taxi/master/CameraImages/100028.jpg', model, 1);

function test(imname, model, num_dets)
global b;
cls = model.class;
fprintf('///// Running demo for %s /////\n\n', cls);

% load and display image
im = imread(imname);
clf;
image(im);
axis equal; 
axis on;
title('input image');
disp('input image');
% disp('press any key to continue'); pause;
% disp('continuing...');

% load and display model
model.vis();
disp([cls ' model visualization']);
% disp('press any key to continue'); pause;
% disp('continuing...');

% detect objects
tic;
[ds, bs] = imgdetect(im, model, -1);
toc;
top = nms(ds, 0.5);
top = top(1:min(length(top), num_dets));
ds = ds(top, :);
bs = bs(top, :);
clf;
if model.type == model_types.Grammar
  bs = [ds(:,1:4) bs];
end
showboxes(im, reduceboxes(model, bs));
title('detections');
disp('detections');
% disp('press any key to continue'); pause;
% disp('continuing...');

if model.type == model_types.MixStar
  % get bounding boxes
  bbox = bboxpred_get(model.bboxpred, ds, reduceboxes(model, bs));
  bbox = clipboxes(im, bbox);
  top = nms(bbox, 0.5);
  clf;
  showboxes(im, bbox(top,:));
  file_id = fopen('MATLAB_logs.txt', 'w+');
  fwrite(file_id, '1', 'char*1');
  b = 1;
  %fclose(file_id);
  title('Person detected');
  disp('bounding boxes');
  disp('press any key to continue'); pause;
end

fprintf('\n');

function test1(imname, model, num_dets)
global b;
global c;
cls = model.class;
fprintf('///// Running demo for %s /////\n\n', cls);

% load and display image
im = imread(imname);
clf;
image(im);
axis equal; 
axis on;
title('input image');
disp('input image');
disp('press any key to continue'); pause;
disp('continuing...');

% load and display model
model.vis();
disp([cls ' model visualization']);
disp('press any key to continue'); pause;
disp('continuing...');

% detect objects
tic;
[ds, bs] = imgdetect(im, model, -1);
toc;
top = nms(ds, 0.5);
top = top(1:min(length(top), num_dets));
ds = ds(top, :);
bs = bs(top, :);
clf;
if model.type == model_types.Grammar
  bs = [ds(:,1:4) bs];
end
showboxes(im, reduceboxes(model, bs));
title('detections');
disp('detections');
disp('press any key to continue'); pause;
disp('continuing...');

if model.type == model_types.MixStar
  % get bounding boxes
  bbox = bboxpred_get(model.bboxpred, ds, reduceboxes(model, bs));
  bbox = clipboxes(im, bbox);
  top = nms(bbox, 0.5);
  clf;
  showboxes(im, bbox(top,:));
  file_id = fopen('MATLAB_logs.txt', 'w+');
  fwrite(file_id, '0', 'char*1');
  fclose(file_id);
  c = 1
  title('Dog detected');
  disp('bounding boxes');
  disp('press any key to continue'); pause;
% end
% if (b+c)==2
%   file_id = fopen('MATLAB_logs.txt', 'w+');
%   fwrite(file_id, '2', 'char*1');
%   fclose(file_id);
end
fprintf('\n');